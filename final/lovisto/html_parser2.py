from bs4 import BeautifulSoup
import urllib.request
from .models import Aportacion, Otro

def html_parser2(url):
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')
    meta = soup.find("meta", {"property": "og:title"})
    if (meta == None):
        titulo = soup.find('title')
        if (titulo != None):
            titulo = titulo.string
        else:
            titulo = "Información extendida no disponible"
        aport = Aportacion.objects.get(url=url)
        O = Otro(aportacion=aport, titulo=titulo)
        O.save()
    else:
        titulo = meta['content']
        img = soup.find("meta", {"property": "og:image"})['content']
        aport = Aportacion.objects.get(url=url)
        O = Otro(aportacion=aport, titulo=titulo, imagen=img)
        O.save()