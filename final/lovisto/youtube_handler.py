import json
import urllib.request
from .models import Youtube, Aportacion

def ytb_js_handler(url, url_org):

    with urllib.request.urlopen(url) as json_doc:
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        objetos = json.loads(json_str)

        aport = Aportacion.objects.get(url=url_org)
        Y = Youtube(aportacion=aport, titulo=objetos['title'], autor=objetos['author_name'], video=objetos['html'])
        Y.save()
