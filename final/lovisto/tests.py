from django.test import TestCase
from .html_parser import html_parser
from .img_parser import img_parser
from django.test import Client

# Create your tests here.
class ParserTests(TestCase):
    def test_html_parser(self):
        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        xml = html_parser(url)
        self.assertEquals(xml, "https://www.aemet.es/xml/municipios/localidad_28065.xml")

    def test_img_parser(self):
        json = "https://es.wikipedia.org/w/api.php?action=query&titles=Astronauta&prop=pageimages&format=json&pithumbsize=100"
        imagen = img_parser(json, "3323")
        self.assertEquals(imagen, "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Bruce_McCandless_II_during_EVA_in_1984.jpg/100px-Bruce_McCandless_II_during_EVA_in_1984.jpg")

class GetTests(TestCase):
    def test_root(self):
        """Check root resource"""

        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3>Lista de aportaciones</h3>', content)

    def test_todo(self):
        c = Client()
        response = c.get('/todo')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3>Lista de aportaciones</h3>', content)

    def test_info(self):
        c = Client()
        response = c.get('/info')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('El registro es totalmente', content)

