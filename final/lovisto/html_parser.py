from bs4 import BeautifulSoup
import urllib.request

def html_parser(url):
    objeto = ""
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')
    if ("aemet" in url):
        for div in soup.find_all('div', class_='enlace_xml inline_block'):
            objeto = "https://www.aemet.es" + div.find('a')['href']

    elif ("youtube" in url):
        objeto = soup.find("link", {"type": "application/json+oembed"})['href']

    elif ("wikipedia" in url):
        objeto = soup.find("h1", {"id": "firstHeading"}).text
        if (" " in objeto):
            objeto = objeto.replace(" ", "_")

    return objeto


