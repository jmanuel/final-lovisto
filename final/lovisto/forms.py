from django import forms
from django.contrib.auth.models import User
from .models import Aportacion, Comentario

class AportacionForm(forms.ModelForm):

    class Meta:
        model = Aportacion
        fields = ('titulo', 'url', 'desc')

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'password',)

class ComentarioForm(forms.ModelForm):

    class Meta:
        model = Comentario
        fields = {'cuerpo'}
