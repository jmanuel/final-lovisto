import json
import urllib.request

def img_parser(url, pag_id):

    with urllib.request.urlopen(url) as json_doc:
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        objetos = json.loads(json_str)
        img = objetos["query"]["pages"][pag_id]["thumbnail"]["source"]

    return img
