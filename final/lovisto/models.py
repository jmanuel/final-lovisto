from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
class Perfil(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    modo = models.CharField(max_length=1, default="n")
    def __str__(self):
        return self.usuario.username

class Aportacion(models.Model):
    user = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=64)
    url = models.URLField(max_length=200)
    desc = models.TextField(default = "")
    fecha = models.DateTimeField()
    votos_pos = models.IntegerField(default=0)
    votos_neg = models.IntegerField(default=0)
    ncomentarios = models.IntegerField(default=0)
    def __str__(self):
        return str(self.id)

class Aemet(models.Model):
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE, default=None)
    municipio = models.CharField(max_length=64)
    provincia = models.CharField(max_length=64)
    copyright = models.TextField()

class Dia(models.Model):
    dia = models.CharField(max_length=64)
    temp_max = models.CharField(max_length=64)
    temp_min = models.CharField(max_length=64)
    sens_max = models.CharField(max_length=64)
    sens_min = models.CharField(max_length=64)
    hum_max = models.CharField(max_length=64)
    hum_min = models.CharField(max_length=64)
    aemet = models.ForeignKey(Aemet, on_delete=models.CASCADE, default=None)

class Youtube(models.Model):
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE, default=None)
    titulo = models.CharField(max_length=64)
    autor = models.CharField(max_length=64)
    video = models.CharField(max_length=500)

class Wikipedia(models.Model):
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE, default=None)
    articulo = models.CharField(max_length=64)
    texto = models.TextField()
    imagen = models.URLField()

class Otro(models.Model):
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE, default=None)
    titulo = models.CharField(max_length=64, default="Información extendida no disponible")
    imagen = models.CharField(max_length=64, default="")

class Comentario(models.Model):
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()

class Aportacion_votada(models.Model):
    usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE)
    voto = models.IntegerField(default=0)

