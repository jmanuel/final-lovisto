from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
from .models import Wikipedia, Aportacion
from .img_parser import img_parser

class TxtHandler(ContentHandler):
    """Clase para:
    Obtener contenido de un documento XML
    """

    def __init__(self):

        self.inApi = False
        self.inContent = False

        self.content = ""
        self.texto = ""
        self.titulo = ""
        self.pageid = ""

    def startElement (self, name, attrs):
        if name == 'api':
            self.inApi = True

        elif self.inApi:
            if name == 'extract':
                self.inContent = True
            if name == 'page':
                self.titulo = attrs.get('title')
                self.pag_id = attrs.get('pageid')

    def endElement (self, name):
        if name == 'api':
            self.inApi = False
            if (" " in self.titulo):
                self.titulo = self.titulo.replace(" ", "_")
            url = "https://es.wikipedia.org/wiki/" + self.titulo
            try:
                aport = Aportacion.objects.get(url=url)
            except Aportacion.DoesNotExist:
                url = url.replace("https:", "http:")
                aport = Aportacion.objects.get(url=url)

            img = img_parser("https://es.wikipedia.org/w/api.php?action=query&titles={articulo}&prop=pageimages&format=json&pithumbsize=100".replace("{articulo}", self.titulo), self.pag_id)
            W = Wikipedia(aportacion=aport, articulo=self.titulo, texto=self.texto, imagen=img)
            W.save()

        elif self.inApi:
            if name == 'extract':
                self.texto = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class TxtParser:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = TxtHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)