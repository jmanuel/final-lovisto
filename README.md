# Final Lo Visto

Práctica final del curso 2020/21

## Datos
* Nombre: Juan Manuel Ruiz Casado
* Titulación: Grado en Tecnologías de la Telecomunicación
* Despliegue(url): http://jmanuel2901.pythonanywhere.com/
* Video básico(url): https://www.youtube.com/watch?v=PEy9-mNTupw
* Video parte opcional(url): https://www.youtube.com/watch?v=u0Zt91QjXtw
 
## Cuenta admin site
* admin/admin

## Cuentas usuarios
* jmanuel/juanma99
* pepe/juanma99

## Lista partes opcionales
* Inclusión de un favicon del sitio
* Visualización de cualquier página en formato JSON y/o XML, de forma similar a como se ha indicado para la página principal.
* Generación de un documento XML y/o JSON para los comentarios puestos en el sitio.
